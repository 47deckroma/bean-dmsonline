package hdi.dmsOnline;

import java.util.Properties;

import it.adobe.deck47.ReadConfigFile;


public class ConfigurationKeys {

	public enum KEYS{ EXT_WS_WEBRAINBOW_URL, EXT_WS_WEBRAINBOW_USER, EXT_WS_WEBRAINBOW_PASSWORD};

	//private static final ConfigurationKeys instance = new ConfigurationKeys();
	private static Properties props;
	//private constructor to avoid client applications to use constructor
	private ConfigurationKeys(){
		ReadConfigFile p = new ReadConfigFile();
		Properties propsConfigFile = p.getProperty(true);		
		props = new Properties();
		props.setProperty(KEYS.EXT_WS_WEBRAINBOW_URL.name(), propsConfigFile.getProperty("DMSONLINE_ULR").trim());
		props.setProperty(KEYS.EXT_WS_WEBRAINBOW_USER.name(), propsConfigFile.getProperty("DMSONLINE_USER").trim());
		props.setProperty(KEYS.EXT_WS_WEBRAINBOW_PASSWORD.name(), propsConfigFile.getProperty("DMSONLINE_PASSWORD").trim());

		//CERTIFICAZIONE
		//props.setProperty(KEYS.EXT_WS_WEBRAINBOW_URL.name(), "http://172.16.230.203:8080/hdi/WebRainbow/Document");
		//props.setProperty(KEYS.EXT_WS_WEBRAINBOW_USER.name(), "admin");
		//props.setProperty(KEYS.EXT_WS_WEBRAINBOW_PASSWORD.name(), "p@ssw0rdHDI-test");
	}

	public static ConfigurationKeys getSingleton(){
		//return instance;
		return new ConfigurationKeys();
	}

	public void setParamValue(KEYS key,String value) {
		props.setProperty(key.name(),value) ;
	}

	public String getParamValue(KEYS key) {
		return  props.getProperty(key.name()) ;
	}

}
