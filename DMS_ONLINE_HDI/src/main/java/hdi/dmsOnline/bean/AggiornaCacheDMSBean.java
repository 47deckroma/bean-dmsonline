package hdi.dmsOnline.bean;

import java.io.Serializable;

public class AggiornaCacheDMSBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8331570424757571189L;
	
	private String code = null;
	private String description = null;
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

}
