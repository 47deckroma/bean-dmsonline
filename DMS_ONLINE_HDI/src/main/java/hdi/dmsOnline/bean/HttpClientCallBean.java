package hdi.dmsOnline.bean;

import java.io.Serializable;

public class HttpClientCallBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8077398470754471549L;

	private String contentBody = null;
	private int statusCode = 0;
	private String statusMsg = null;

	public String getContentBody() {
		return contentBody;
	}

	public void setContentBody(String contentBody) {
		this.contentBody = contentBody;
	}
	
	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

}
