package hdi.dmsOnline;

import it.peng.wr.webservice.DocumentServicePortBindingStub;
import it.peng.wr.webservice.DocumentServiceProxy;
import it.peng.wr.webservice.FieldData;
import it.peng.wr.webservice.QueryCondition;
import it.peng.wr.webservice.WebServiceException;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.axis.AxisFault;

import hdi.dmsOnline.ConfigurationKeys.KEYS;

public class WebRainbowUtility {
	
	private static final Logger log = Logger.getLogger( WebRainbowUtility.class.getName() );
	
    private DocumentServiceProxy proxy = null;
    private String token = null;

    private static int TIMEOUT_WEBRAINBOW = 6000;
    private static int TIMEOUT_REQUEST = 250;
    private static int ATTEMPT_REQUEST = 2;

    public static final String entityType_FOLDER_MOVIMENTO_POLIZZA = "wr:folderMovimentoPolizza";

    public static final String entityType_FOLDER_FOGLIO_CASSA = "wr:folderFogliCassaDecadale";
    public static final String name_FOLDER_FOGLIO_CASSA = "Fogli Cassa Decadale";
    
    public static final String entityType_FOLDER_BUSTA = "wr:folderBusta";
    
    public static final String entityType_FOGLIO_CASSA = "wr:fogliCassaDecadale";
    public static final String entityType_MOVIMENTO_POLIZZA = "wr:movimentoPolizza";
    public static final String entityType_MOVIMENTO_POLIZZA_VALIDATO = "wr:movimentoPolizzaValidato";
    
    public static final String entityType_BUSTA = "wr:busta";
    public static final String entityType_DOCUMENTO = "wr:documentoCompleto";
    public static final String entityType_DOCUMENTO_IDM = "wr:documentoidm";
    public static final String entityType_ALLEGATO_FOGLI_CASSA_DECADALE = "wr:allegatoFogliCassaDecadale";

    //RDB aggiunta per ditinta resi
    public static final String entitytype_FOLDER_DISTINTA_RESI = "wr:folderDistintaResi";
    public static final String name_FOLDER_DISTINTA_RESI = "Distinta resi";
    
    public static final String entityType_DISTINTA_RESI = "wr:distintaResi";
    public static final String entityType_ALLEGATO_DISTINTA_RESI = "wr:allegatoDistintaResi";
    
    public static final String entityType_FASCICOLO = "wr:fascicolo";
    
    private static String prop_PREFIX = "wrprop:";
    private static String jcr_PREFIX = "jcr:";

    private static String prop_NAME = prop_PREFIX + "name";
    private static String prop_ORIGINAL_FILE_NAME = prop_PREFIX + "originalFilename";
    private static String prop_FILE_EXTENSION = prop_PREFIX + "fileExtension";
    private static String prop_DESCRIPTION = prop_PREFIX + "description";
    private static String prop_CODICE_AGENZIA = prop_PREFIX + "codiceAgenzia";
    private static String prop_CODICE_DOCUMENTO = prop_PREFIX + "codiceDocumento";
    private static String prop_PERIODO_INIZIO = prop_PREFIX + "periodoInizio";
    private static String prop_PERIODO_FINE = prop_PREFIX + "periodoFine";
    private static String prop_TIPOLOGIA_DOCUMENTO = prop_PREFIX  + "tipologiaDocumento";
    private static String prop_NUMERO_POLIZZA = prop_PREFIX + "numeroPolizza";
    private static String prop_ID_MOVIMENTO = prop_PREFIX + "idMovimento";
    private static String prop_CODICE_CAUSALE = prop_PREFIX + "codiceCausale";
    private static String prop_AMBIENTE_RIFERIMENTO = prop_PREFIX + "ambienteRiferimento";

    //wrprop:wf_state solo su wr:movimentoPolizzaValidato
    private static String prop_ID_FASCICOLO = prop_PREFIX + "idFascicolo";
    private static String prop_WF_STATE = prop_PREFIX + "wf_state";
    private static String prop_ESITO_CONTROLLI = prop_PREFIX + "esitoControlli";
    private static String prop_DATA_SCANSIONE = prop_PREFIX + "dataScansione";
    private static String prop_DATA_CREAZIONE = prop_PREFIX + "creationDate";
    
    private static String prop_ID_IDM = prop_PREFIX + "idIdm";
    
    private static String prop_RAMO = prop_PREFIX + "ramo";
    
    private static String prop_STATO_PTF = prop_PREFIX + "statoPTF";
    
    public static final String STATE_IN_ATTESA_DI_VALIDAZIONE = "In attesa validazione";
    public static final String STATE_VERIFICA_CLUSTER ="verifica cluster";
    public static final String STATE_RICHIESTA_DOCUMENTAZIONE_AGGIUNTIVA = "richiesta documentazione aggiuntiva";
    public static final String STATE_SCARTATO = "scartato";
    public static final String STATE_VALIDATO = "validato";
    public static final String STATE_IN_ATTESA_STATO_PTF = "in attesa stato PTF";


    private static String jcr_UUID = jcr_PREFIX + "uuid";
    private static String jcr_MIME_TYPE = jcr_PREFIX + "mimeType";
    private static String jcr_DATA = jcr_PREFIX + "data";
    private static String jcr_PRIMARY_TYPE = jcr_PREFIX + "primaryType";
    
    public static final int IDX_UUID = 0;
    public static final int IDX_CODICE_DOCUMENTO = 1;
    public static final int IDX_DESCRIZIONE_DOCUMENTO = 2;
    public static final int IDX_ID_MOVIMENTO = 3;
    public static final int IDX_NUMERO_POLIZZA = 4;
    public static final int IDX_UUID_DOC = 5;
    public static final int IDX_UUID_IDM = 6;
    public static final int IDX_ID_FASCICOLO = 7;
    //public static final int IDX_DA_VERIFICARE = 8;
    public static final int IDX_STATO = 8;
    public static final int IDX_ESITO_CONTROLLI = 9;
    public static final int IDX_ID_IDM = 10;
    public static final int IDX_DATA_SCANSIONE = 11;
    public static final int IDX_DATA_CREAZIONE = 12;
    
    
    public static final int NUMBER_IDX = IDX_DATA_CREAZIONE+1;
    
    

    private DocumentServiceProxy getProxy() throws RemoteException {
        if (proxy == null) {
            proxy = new DocumentServiceProxy();
            DocumentServicePortBindingStub stub = (DocumentServicePortBindingStub) proxy.getDocumentService();
            stub.setMaintainSession(true);
            stub.setTimeout(TIMEOUT_WEBRAINBOW);
            // System.out.println("Timeout: "+ stub.getTimeout());
            ConfigurationKeys cfg = ConfigurationKeys.getSingleton();
            String urlWs = cfg.getParamValue(KEYS.EXT_WS_WEBRAINBOW_URL);
            String user = cfg.getParamValue(KEYS.EXT_WS_WEBRAINBOW_USER);
            String password = cfg.getParamValue(KEYS.EXT_WS_WEBRAINBOW_PASSWORD);
            /*
            log.severe("************************************************************************************" );
            log.severe("ConfigurationKeys: EXT_WS_WEBRAINBOW_URL = " + urlWs + "end string");
            log.severe("ConfigurationKeys: EXT_WS_WEBRAINBOW_USER = " + user + "end string");
            log.severe("ConfigurationKeys: EXT_WS_WEBRAINBOW_PASSWORD = " + password + "end string");
            log.severe("************************************************************************************" );
            */
            if (urlWs != null && user != null && password != null) {
                proxy.setEndpoint(urlWs);
                loginProxy(user, password);
            }
        }
        return proxy;
    }

    private String getToken() throws RemoteException {
        if (token == null) {
            proxy = getProxy();
        }
        return token;
    }

    private void loginProxy(String user, String password) throws RemoteException {
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
                token = proxy.login(user, password, null);
                break;
            } catch (RemoteException e) {
                sleepOrError("loginProxy",attempt,e);
            }
        }
    }

    public void logoutProxy() throws RemoteException {
        if (proxy != null && token != null) {
            for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
                try {
                    proxy.logout(token);
                    break;
                } catch (RemoteException e) {
                    sleepOrError("logoutProxy",attempt,e);
                }
            }
        }
        proxy = null;
        token = null;
    }

    public List<String[]> wsRlGetDocumentsOfEnvelope(String idBusta) throws RemoteException {
        String ruleName = "wsRlGetDocumentsOfEnvelope";
        ParameterField par = new ParameterField();
        par.put("codiceBusta", idBusta);
        par.put("sortingBy", "");
        par.put("sortingMode","ascending");
        String xml = eseguiRegola(ruleName,par.toFieldDataArray());
        int record = getCountRecordXml(xml);
        List<String[]> toReturn = new ArrayList<String[]>();
        for (int i = 0; i < record; i++) {
            String[] tupla = new String[NUMBER_IDX];
            tupla[IDX_UUID]= getFromXml(xml,jcr_UUID,i);
            tupla[IDX_CODICE_DOCUMENTO]= getFromXml(xml,prop_CODICE_DOCUMENTO,i);
            tupla[IDX_DESCRIZIONE_DOCUMENTO]= getFromXml(xml,prop_TIPOLOGIA_DOCUMENTO,i);
            tupla[IDX_ID_MOVIMENTO]= getFromXml(xml,prop_ID_MOVIMENTO,i);
            tupla[IDX_NUMERO_POLIZZA]= getFromXml(xml,prop_NUMERO_POLIZZA,i);
            
            String[] documenti = getDocumentiEntityFromContainer(tupla[IDX_UUID], null);
			
			if(documenti != null && documenti.length > 0){
				for(String documento: documenti){
					String type = getDocumentType(documento);
					if(type != null && type.equals(entityType_DOCUMENTO)){
						 tupla[IDX_UUID_DOC] = documento;
					}else if(type != null && type.equals(entityType_DOCUMENTO_IDM)){
						tupla[IDX_UUID_IDM] = documento;
					}
				}
			}
			boolean isDocumentoValidato = entityType_MOVIMENTO_POLIZZA_VALIDATO.equals(getProxy().getEntityType(getToken(), tupla[IDX_UUID]));  
			
			String stato = null;
			if (isDocumentoValidato){
				Object[] fascicolo = getProxy().getFieldValue(getToken(), tupla[IDX_UUID], prop_ID_FASCICOLO);
				if (fascicolo!= null && fascicolo.length>0){ 
					tupla[IDX_ID_FASCICOLO]= (String)getProxy().getFieldValue(getToken(), tupla[IDX_UUID], prop_ID_FASCICOLO)[0];
				}
			
				stato =(String)getProxy().getFieldValue(getToken(), tupla[IDX_UUID], prop_WF_STATE)[0];
    			tupla[IDX_STATO] = stato==null?null:stato.toUpperCase();
    			
    			String esitoControlli = getFromXml(xml,prop_ESITO_CONTROLLI,i);
    			tupla[IDX_ESITO_CONTROLLI]= esitoControlli==null?null:esitoControlli.toUpperCase();
                
    			String idIDM = getFromXml(xml,prop_ID_IDM,i);
    			tupla[IDX_ID_IDM]= idIDM==null?null:idIDM.toUpperCase();
    		}
			
			//String dataRicezione = getFromXml(xml,prop_DATA_RICEZIONE,i);
			//tupla[IDX_ESITO_CONTROLLI]= dataRicezione==null?null:dataRicezione.toUpperCase();
            
            
            /*
            String[] doc = getProxy().getEntityChildren(getToken(), tupla[IDX_UUID], entityType_DOCUMENTO);
            if (getProxy().getFieldValue(getToken(), doc[0], jcr_PRIMARY_TYPE)[0].equals(entityType_DOCUMENTO)){
                if (doc !=null && doc.length>0){
                    tupla[IDX_UUID_DOC] =  doc[0];
                }
            }
            
            String[] docIdm = getProxy().getEntityChildren(getToken(), tupla[IDX_UUID], entityType_DOCUMENTO_IDM);
            if (getProxy().getFieldValue(getToken(), docIdm[0], jcr_PRIMARY_TYPE)[0].equals(entityType_DOCUMENTO_IDM)){
                if (docIdm !=null && docIdm.length>0){
                    tupla[IDX_UUID_IDM] =  docIdm[0];
                }
            }
            */
			if (!isDocumentoValidato || (!STATE_IN_ATTESA_DI_VALIDAZIONE.equalsIgnoreCase(stato) && !STATE_SCARTATO.equalsIgnoreCase(stato))){
				toReturn.add(tupla);
			}
        }
        //ordinamento
        Collections.sort(toReturn, new Comparator<String[]>() {
              //  @Override
                public int compare(String[]  tupla1, String[]  tupla2)
                {
    				int sComp = tupla1[IDX_ID_FASCICOLO]==null||tupla2[IDX_ID_FASCICOLO]==null?0:tupla1[IDX_ID_FASCICOLO].compareTo(tupla2[IDX_ID_FASCICOLO]);
    				if (sComp != 0) {
    					return sComp;
    				} else {
    					return tupla1[IDX_ID_IDM]==null||tupla2[IDX_ID_IDM]==null?0:tupla1[IDX_ID_IDM].compareTo(tupla2[IDX_ID_IDM]);
    				}
                }
            });
        
        return toReturn;
    }
    
    private int getCountRecordXml(String xml){
        String tagRecord = "<record>";
        int lastIndex = 0;
        int count = 0;
        while(lastIndex != -1){
            lastIndex = xml.indexOf(tagRecord,lastIndex);
            if(lastIndex != -1){
                count ++;
                lastIndex += tagRecord.length();
            }
        }
        return count;
    }
    
    private String getFromXml(String xml,String key,int record){
        String toReturn = null;
        String tagRecord = "<record>";
        int lastIndex = 0;
        int recordCount = -1;
        while(lastIndex != -1){
            lastIndex = xml.indexOf(tagRecord,lastIndex);
            if(lastIndex != -1){
                recordCount ++;
                lastIndex += tagRecord.length();
                if (recordCount == record){
                    break;
                }
            }
        }
        int startKey= xml.indexOf("<key>"+key+"</key>",lastIndex);
        if (startKey>=0){
            int start = xml.indexOf("<value>",startKey)+7;
            int end = xml.indexOf("</value>",startKey);
            toReturn = xml.substring(start,end); 
        }
        return toReturn;
        
    }
 
    public String wsRlGetResourcesIDM(String idMovimento) throws RemoteException {
        String ruleName = "wsRlGetResourcesIDM";
        ParameterField par = new ParameterField();
        par.put("documentId",idMovimento);
        return eseguiRegola(ruleName,par.toFieldDataArray());
    }
    
    public String wsRlGetDocumentFields(String idDocumento) throws RemoteException {
        String ruleName = "wsRlGetDocumentFields";
        ParameterField par = new ParameterField();
        par.put("documentId",idDocumento);
        return eseguiRegola(ruleName,par.toFieldDataArray());
    }
    
    public String getUUI(String entityType, Map<String,String> parametri) throws RemoteException {
        String uuidFolder = null;
        QueryCondition[] qc = null;
        if (parametri != null){
            qc = new QueryCondition[parametri.size()];
            int i = 0;
            for (String key : parametri.keySet()) {
                qc[i] = new QueryCondition("equal",key,parametri.get(key));
                i++;
            }
        }
        String[] entitiesFolderList = null;
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
                entitiesFolderList = getProxy().findEntities(getToken(), entityType, null, qc);
                break;
            } catch (RemoteException e) {
               sleepOrError("getUUI",attempt,e);
            }
        }
        if (entitiesFolderList != null && entitiesFolderList.length > 0)
            uuidFolder = entitiesFolderList[0];

        return uuidFolder;
    }

    public String addDocumento(WebRainbowDocument wrDoc) throws RemoteException {
        
        Map<String,String> parametri = new HashMap<String,String>();
        if (wrDoc.folderName!=null && !wrDoc.folderName.trim().isEmpty()){
        	parametri.put(WebRainbowUtility.prop_NAME, wrDoc.folderName);
        }
        else{
        	parametri.put(WebRainbowUtility.jcr_PRIMARY_TYPE, wrDoc.folderEntityType);
        }

        String entityIdFolder = getUUI(wrDoc.folderEntityType, parametri);
        if (entityIdFolder==null || entityIdFolder.trim().isEmpty()){
        	parametri = new HashMap<String,String>();
            parametri.put(WebRainbowUtility.jcr_PRIMARY_TYPE, wrDoc.folderEntityType);
            entityIdFolder = getUUI(wrDoc.folderEntityType, parametri);
        }

        FieldData[] containerFields = wrDoc.getContainerFields();
        FieldData[] documentFields = wrDoc.getDocumentFields();
        
        for (FieldData fieldData : containerFields) {
            if (fieldData.getFieldName().equals(prop_TIPOLOGIA_DOCUMENTO)){
                   Map<String,String> param = new HashMap<String,String>();
                    param.put("wrprop:codice", (String)fieldData.getFieldValue()[0]);
             String[] result = findEntities("wr:tipologiaDocumento", param);
             if (result!=null && result.length > 0){
                   fieldData.setFieldValue(result);
             }
             break;
            }
        }

	    System.out.println("WebRainbowUtility: addDocumento()");

        String entityIdContainer = getProxy().fastInsertEntity(getToken(), wrDoc.containerEntityType, containerFields, entityIdFolder);
        String entityIdDocument = getProxy().fastInsertEntity(getToken(), wrDoc.documentEntityType, documentFields, entityIdContainer);
        return entityIdDocument;
    }

    public String signDocument(String userName, String userPwd, String otp, String documentEntityId) throws RemoteException {
        String ruleName = "gnRlSignEntities";
        ParameterField par = new ParameterField();
        par.put("userName", userName);
        par.put("userPwd", userPwd);
        par.put("OTP",otp);
        par.put("entityList",documentEntityId);
        String s = eseguiRegola(ruleName,par.toFieldDataArray());
        return s;
    }

    public byte[] getDocumento(String documentEntityId) throws RemoteException {
        Object[] data = null;
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
                data = getProxy().getFieldValue(getToken(), documentEntityId, jcr_DATA);
                break;
            } catch (RemoteException e) {
               sleepOrError("getDocumento",attempt,e);
            }
        }
        byte[] pdf = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            pdf[i] = ((Byte) data[i]).byteValue();
        }
        return pdf;
    }

    public String getDocumentoEntityFromContainer(String containerEntityId,String entityTypeDocumento) throws RemoteException {
        Object[] data = null;
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
                data = getProxy().getEntityChildren(getToken(), containerEntityId, entityTypeDocumento);
                break;
            } catch (RemoteException e) {
               sleepOrError("getDocumentoEntityFromContainer",attempt,e);
            }
        }
        return data==null?null:(String)data[0];
    }

    public String[] getDocumentiEntityFromContainer(String containerEntityId,String entityTypeDocumento) throws RemoteException {
        String[] data = null;
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
                data = getProxy().getEntityChildren(getToken(), containerEntityId, entityTypeDocumento);
                break;
            } catch (RemoteException e) {
               sleepOrError("getDocumentoEntityFromContainer",attempt,e);
            }
        }
        return data==null?null:(String[])data;
    }
    
    public String getContainerEntityFromDocument(String documentEntityId) throws RemoteException {
        String data = null;
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
                data = getProxy().getParentEntity(getToken(), documentEntityId);
                break;
            } catch (RemoteException e) {
               sleepOrError("getContainerEntityFromDocument",attempt,e);
            }
        }
        return data;
    }

    
    private String eseguiRegola(String ruleName,FieldData[] parametri) throws RemoteException{
        String toReturn = null;
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
                toReturn = getProxy().executeRule(getToken(), ruleName, parametri);
                break;
            } catch (RemoteException e) {
               sleepOrError(ruleName,attempt,e);
            }
        }
        return toReturn;
    }
    
    private void sleepOrError(String label, int attempt, RemoteException e) throws RemoteException{
        if  (e instanceof AxisFault && !(e instanceof WebServiceException)){
            if (attempt == ATTEMPT_REQUEST) {
                //System.out.println(label + " - Tenativo " + attempt + " Fallito. Rilancio l'errore");
                throw e;
            } else {
                //System.out.println(label + " - Tenativo " + attempt + " di " + ATTEMPT_REQUEST + " Fallito. Aspetto " + TIMEOUT_REQUEST + "ms e ritento.");
                try {
                    Thread.sleep(TIMEOUT_REQUEST);
                } catch (InterruptedException t) {
                }
            }
        }
        else {
            throw e;
        }
    }
    
    public static boolean isValidEntityId(String id){
        return id.toLowerCase().matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");
    }
    
    @Override
    protected void finalize() throws Throwable {
        logoutProxy();
        super.finalize();
    }

    class ParameterField extends ArrayList<FieldData> {
        private static final long serialVersionUID = -2949416614512556003L;

        public void put(String property, Object value) {
            FieldData fd = new FieldData();
            fd.setFieldName(property);
            Object[] _value = { value };
            fd.setFieldValue(_value);
            this.add(fd);
        }

        public FieldData[] toFieldDataArray() {
            return (FieldData[]) this.toArray(new FieldData[this.size()]);
        }
    }

    public class WebRainbowDocument {
        protected String folderEntityType;
        protected String folderName;
        protected String containerEntityType;
        protected String documentEntityType = entityType_DOCUMENTO;

        public String fileName;
        public byte[] pdf;

        public FieldData[] getContainerFields() {
            ParameterField field = new ParameterField();
            return field.toFieldDataArray();
        }
        
        private FieldData[] getDocumentFields() {
            ParameterField field = new ParameterField();
            field.put(prop_NAME, fileName);
            field.put(prop_ORIGINAL_FILE_NAME, fileName);
            field.put(jcr_MIME_TYPE, "application/pdf");
            field.put(prop_FILE_EXTENSION, ".pdf");
            field.put(prop_DESCRIPTION, "");
            field.put(jcr_DATA, pdf);
            return field.toFieldDataArray();
        }
        
    }
    
 
 public class WebRainbowDocumentMovimentoPolizza extends WebRainbowDocument{
        
        public String codiceAgenzia;
        public String codiceDocumento;
        public String numeroPolizza;
        public String idMovimento;
        public String tipologiaDocumento;
        public String codiceCausale;
        public String ambienteRiferimento;

        public WebRainbowDocumentMovimentoPolizza(String codiceAgenzia,String codiceDocumento, String numeroPolizza,String idMovimento, String tipologiaDocumento,String codiceCausale,String ambienteRiferimento){
            folderEntityType =  "wr:folderMovimentoPolizza"; 
            folderName = "Movimenti Polizze"; 
            containerEntityType = "wr:movimentoPolizza"; 
            documentEntityType = "wr:documentoCompleto"; 
            
            this.codiceAgenzia=codiceAgenzia;
            this.codiceDocumento = codiceDocumento;
            this.numeroPolizza = numeroPolizza;
            this.idMovimento = idMovimento;
            this.tipologiaDocumento = tipologiaDocumento;
            this.codiceCausale = codiceCausale;
            this.ambienteRiferimento = ambienteRiferimento;
            
        }
        
        public FieldData[] getContainerFields() {

            ParameterField field = new ParameterField();
                
            //wrprop:tipologiaDocumento -> tipologiaDocumento 
            if (tipologiaDocumento != null) {
            	//@Paolo : se commento il put della properties l'invocazione va a buon fine.
                field.put(prop_TIPOLOGIA_DOCUMENTO, tipologiaDocumento);
            }
          //wrprop:numeroPolizza -> numeroPolizza
            if (numeroPolizza != null) {
                field.put(prop_NUMERO_POLIZZA, numeroPolizza);
            }
            if (codiceAgenzia != null) {
                field.put(prop_CODICE_AGENZIA, codiceAgenzia);
            }
            if (codiceDocumento != null) {
                field.put(prop_CODICE_DOCUMENTO, codiceDocumento);
            }
          //wrprop:idMovimento ->  idMovimento
	          if (idMovimento != null) {
	        	  field.put(prop_ID_MOVIMENTO, idMovimento);
	          }
	          //wrprop:codiceCausale ->  codiceCausale
	          if (codiceCausale != null) {
	        	  field.put(prop_CODICE_CAUSALE, codiceCausale);
	          }
	          //wrprop:ambienteRiferimento ->  ambienteRiferimento
	          if (ambienteRiferimento != null) {
	        	  field.put(prop_AMBIENTE_RIFERIMENTO, ambienteRiferimento);
	          }
	          
            return field.toFieldDataArray();
        }

    }
 
 
 
    public class WebRainbowDocumentFogliCassa extends WebRainbowDocument{
        
        public String codiceAgenzia;
        public String codiceDocumento;
        public String periodoInizio;
        public String periodoFine;

        public WebRainbowDocumentFogliCassa(){
            folderEntityType = entityType_FOLDER_FOGLIO_CASSA;
            folderName = name_FOLDER_FOGLIO_CASSA; 
            containerEntityType = entityType_FOGLIO_CASSA;
            documentEntityType = entityType_ALLEGATO_FOGLI_CASSA_DECADALE;
        }
        
        public FieldData[] getContainerFields() {

            ParameterField field = new ParameterField();
            if (codiceAgenzia != null) {
                field.put(prop_CODICE_AGENZIA, codiceAgenzia);
            }
            if (codiceDocumento != null) {
                field.put(prop_CODICE_DOCUMENTO, codiceDocumento);
            }
            if (periodoInizio != null) {
                field.put(prop_PERIODO_INIZIO, periodoInizio);
            }
            if (periodoFine != null) {
                field.put(prop_PERIODO_FINE, periodoFine);
            }
            return field.toFieldDataArray();
        }

    }


    
   public class WebRainbowDocumentDistintaResi extends WebRainbowDocument{
        
        public String codiceAgenzia;
        public String codiceDocumento;
        public String periodoInizio;

        
        public WebRainbowDocumentDistintaResi(){
            folderEntityType = entitytype_FOLDER_DISTINTA_RESI;
            folderName = name_FOLDER_DISTINTA_RESI; 
            containerEntityType = entityType_DISTINTA_RESI;
            documentEntityType = entityType_ALLEGATO_DISTINTA_RESI;
        }
        
        public FieldData[] getContainerFields() {

            ParameterField field = new ParameterField();
            if (codiceAgenzia != null) {
                field.put(prop_CODICE_AGENZIA, codiceAgenzia);
            }
            if (codiceDocumento != null) {
                field.put(prop_CODICE_DOCUMENTO, codiceDocumento);
            }
            if (periodoInizio != null) {
                field.put(prop_PERIODO_INIZIO, periodoInizio);
            }
            // necessari per il documento
            if (fileName != null) {
                field.put(prop_NAME, fileName);
            }

            return field.toFieldDataArray();
        }

    }
    
    public String[] findEntities(String entityType, Map<String,String> parametri) throws RemoteException {
        
        QueryCondition[] qc = null;
        if (parametri != null){
            qc = new QueryCondition[parametri.size()];
            int i = 0;
            for (String key : parametri.keySet()) {
                qc[i] = new QueryCondition("equal",key,parametri.get(key));
                i++;
            }
        }
        String[] entitiesFolderList = null;
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
                entitiesFolderList = getProxy().findEntities(getToken(), entityType, null, qc);
                break;
            } catch (RemoteException e) {
               sleepOrError("findEntities",attempt,e);
            }
        }
        return entitiesFolderList;
    }

    public List<String[]> findEntity(String numeroPolizza, String numeroProposta) throws RemoteException {
        
    	String[] uuidPol = null;
    	String[] uuidProp = null; 
        Map<String,String> parametri = new HashMap<String,String>();
        //parametri.put(WebRainbowUtility.prop_RAMO, "VITA");
        if(numeroPolizza != null && !numeroPolizza.equals("")){
	        parametri.put(prop_NUMERO_POLIZZA, numeroPolizza);
	        uuidPol = findEntities(entityType_MOVIMENTO_POLIZZA, parametri);
        }
        if(numeroProposta != null && !numeroProposta.equals("") && !numeroProposta.equals(numeroPolizza)){
	        parametri.put(prop_NUMERO_POLIZZA, numeroProposta);
	        uuidProp = findEntities(entityType_MOVIMENTO_POLIZZA, parametri);
        }
        
        
        String[] listUUID = null;
        if (uuidPol != null && uuidProp !=null){
        	listUUID = Arrays.copyOf(uuidPol, uuidPol.length + uuidProp.length);
        	System.arraycopy(uuidProp, 0, listUUID, uuidPol.length, uuidProp.length);
        }
        else{
        	listUUID = uuidPol != null?uuidPol:uuidProp;
        }
        
        return prepareTuplaForOutput(listUUID);
    }
    
    private List<String[]> prepareTuplaForOutput(String[] listUUID) throws RemoteException{
    	List<String[]> toReturn = new ArrayList<String[]>();
    	if(listUUID != null){
	    	for (String uuid : listUUID) {
	        	
	        	String[] tupla = new String[NUMBER_IDX];
	        	tupla[IDX_UUID] = uuid;
	
	        	Object[] value = getProxy().getFieldValue(getToken(), uuid, prop_CODICE_DOCUMENTO);
				if (value!= null && value.length>0){ 
					tupla[IDX_CODICE_DOCUMENTO]= (String)value[0];
				}
				
				value = getProxy().getFieldValue(getToken(), uuid, prop_ID_MOVIMENTO);
				if (value!= null && value.length>0){ 
					tupla[IDX_ID_MOVIMENTO]= (String)value[0];
				}
				
				value = getProxy().getFieldValue(getToken(), uuid, prop_NUMERO_POLIZZA);
				if (value!= null && value.length>0){ 
					tupla[IDX_NUMERO_POLIZZA]= (String)value[0];
				}
				
				try{
					value = getProxy().getFieldValue(getToken(), uuid, prop_DATA_CREAZIONE);
					if (value!= null && value.length>0){
						Calendar c = (Calendar)value[0];
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						tupla[IDX_DATA_CREAZIONE]= sdf.format(c.getTime());
					}	
				}catch(Exception e){}
				
				boolean isDocumentoValidato = entityType_MOVIMENTO_POLIZZA_VALIDATO.equals(getProxy().getEntityType(getToken(), tupla[IDX_UUID]));
				String stato = null;
				if (isDocumentoValidato){
					
					stato = (String)getProxy().getFieldValue(getToken(), uuid, prop_WF_STATE)[0];
					if (stato != null){ 
						tupla[IDX_STATO]= ((String)stato).toUpperCase();
					}
				
					value = getProxy().getFieldValue(getToken(), uuid, prop_ESITO_CONTROLLI);
					if (value!= null && value.length>0){ 
						tupla[IDX_ESITO_CONTROLLI]= (String)value[0];
					}
	
					value = getProxy().getFieldValue(getToken(), uuid, prop_ID_FASCICOLO);
					if (value!= null && value.length>0){ 
						tupla[IDX_ID_FASCICOLO]= (String)value[0];
					}
	
					value = getProxy().getFieldValue(getToken(), uuid, prop_ID_IDM);
					if (value!= null && value.length>0){ 
						tupla[IDX_ID_IDM]= (String)value[0];
					}
	
					value = getProxy().getFieldValue(getToken(), uuid, prop_DATA_SCANSIONE);
					if (value!= null && value.length>0){
						Calendar c = (Calendar)value[0];
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						tupla[IDX_DATA_SCANSIONE]= sdf.format(c.getTime());
					}
				}
				else{
					value = getProxy().getFieldValue(getToken(), uuid, prop_TIPOLOGIA_DOCUMENTO);
					if (value!= null && value.length>0){ 
						tupla[IDX_DESCRIZIONE_DOCUMENTO]= (String)value[0];
					}
				}
	            String[] documenti = getDocumentiEntityFromContainer(tupla[IDX_UUID], null);
	    			
				if(documenti != null && documenti.length > 0){
					for(String documento: documenti){
						String type = getDocumentType(documento);
						if(type != null && type.equals(entityType_DOCUMENTO)){
							 tupla[IDX_UUID_DOC] = documento;
						}else if(type != null && type.equals(entityType_DOCUMENTO_IDM)){
							tupla[IDX_UUID_IDM] = documento;
						}
					}
				}
				
				if (!isDocumentoValidato || (!STATE_IN_ATTESA_DI_VALIDAZIONE.equalsIgnoreCase(stato) && !STATE_SCARTATO.equalsIgnoreCase(stato))){
					toReturn.add(tupla);
				}
	        }
	        //ordinamento
	        Collections.sort(toReturn, new Comparator<String[]>() {
	                //@Override
	                public int compare(String[]  tupla1, String[]  tupla2)
	                {
	    				int sComp = tupla1[IDX_ID_FASCICOLO]==null||tupla2[IDX_ID_FASCICOLO]==null?0:tupla1[IDX_ID_FASCICOLO].compareTo(tupla2[IDX_ID_FASCICOLO]);
	    				if (sComp != 0) {
	    					return sComp;
	    				} else {
	    					return tupla1[IDX_ID_IDM]==null||tupla2[IDX_ID_IDM]==null?0:tupla1[IDX_ID_IDM].compareTo(tupla2[IDX_ID_IDM]);
	    				}
	                }
	            });
    	}
        return toReturn;	        
    }
    

    public Object[] getFieldValue(String entityId, String prop) throws RemoteException {

        Object[] toReturn = null;
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
            	toReturn = getProxy().getFieldValue(getToken(), entityId, prop);
                break;
            } catch (RemoteException e) {
               sleepOrError("getFieldValue",attempt,e);
            }
        }
        return toReturn ;
    }

    public String getDocumentType(String entityId) throws RemoteException {

        Object[] toReturn = null;
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
            	toReturn = getProxy().getFieldValue(getToken(), entityId, jcr_PRIMARY_TYPE);
                break;
            } catch (RemoteException e) {
               sleepOrError("getFieldValue",attempt,e);
            }
        }
        return toReturn==null?null:(String)toReturn[0];
    }
    
    public void updateStatePtfFascicolo(String numeroProposta) throws RemoteException {

    	String[] uuidFasc = null; 
        Map<String,String> parametri = new HashMap<String,String>();

        if(numeroProposta != null && !numeroProposta.equals("")){
	        parametri.put(prop_NUMERO_POLIZZA, numeroProposta);
	        uuidFasc = findEntities(entityType_FASCICOLO, parametri);
        }
        	
        if(uuidFasc != null && uuidFasc.length > 0){
        	ParameterField field = new ParameterField();
        	field.put(prop_STATO_PTF, true);
        	FieldData[] fd = field.toFieldDataArray();
        	
        	for(String uuid: uuidFasc){
        		try{
        			updateEntity(uuid, fd);
        		}catch(Exception e){}
        	}
        		
        }
    }
    
    private void updateEntity(String entityId, FieldData[] fd) throws RemoteException {

        String toReturn = null;
        for (int attempt = 1; attempt <= ATTEMPT_REQUEST; attempt++) {
            try {
            	toReturn = getProxy().fastUpdateEntity(getToken(), entityId, fd);
                break;
            } catch (RemoteException e) {
               sleepOrError("updateEntity",attempt,e);
            }
        }
    }
    
}
