package hdi.dmsOnline;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.Base64;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.xml.sax.InputSource;

import com.adobe.idp.Document;

import hdi.dmsOnline.ConfigurationKeys.KEYS;
import hdi.dmsOnline.WebRainbowUtility.WebRainbowDocumentMovimentoPolizza;
import hdi.dmsOnline.bean.AggiornaCacheDMSBean;
import hdi.dmsOnline.bean.HttpClientCallBean;
import it.adobe.deck47.ReadConfigFile;

public class Operation_DMSONLINE {

	private static final Logger log = Logger.getLogger( Operation_DMSONLINE.class.getName() );


	private static byte[] getbyte(Document doc) throws IOException{
		byte[] byteArray = null;
		ByteArrayOutputStream buffer =null;
		InputStream is=null;
		try {
			buffer = new ByteArrayOutputStream();
			int nRead;
			is = doc.getInputStream()  ;
			byte[] data = new byte[16384];
			while ((nRead = is.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			buffer.flush();
			byteArray = buffer.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(buffer!=null)buffer.close();
			if(is!=null)is.close();
		}

		return byteArray;
	}
	//return dfc3beb8-c574-41f8-8c6c-2e577265385e
	public static String addMovimentoPolizza(Document doc,String fileName,String codiceAgenzia,String codiceDocumento, String numeroPolizza,String idMovimento, String tipologiaDocumento,String codiceCausale) throws IOException,Exception {
		String returnValue = "";
		try {
			WebRainbowUtility util = new WebRainbowUtility();
			ReadConfigFile p = new ReadConfigFile();
			Properties props = p.getProperty(true);
			//DMSONLINE_ULR=http://172.16.230.177:8080/WebRainbow4811/Document
			//DMSONLINE_USER=UNICO
			//DMSONLINE_PASSWORD=password
			System.out.println("DMSONLINE: Connessione a :"+props.getProperty("DMSONLINE_ULR"));
			//ConfigurationKeys.getSingleton().setParamValue(KEYS.EXT_WS_WEBRAINBOW_URL, props.getProperty("DMSONLINE_ULR") );
			WebRainbowDocumentMovimentoPolizza wrDo = util.new WebRainbowDocumentMovimentoPolizza(codiceAgenzia,codiceDocumento,numeroPolizza,idMovimento,tipologiaDocumento,codiceCausale,p.getPropertyValue(false, "TipoAmbiente"));
			wrDo.fileName =fileName;
			wrDo.pdf = getbyte(doc);

			returnValue = util.addDocumento(wrDo);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return returnValue;
	}


	private static HttpClientCallBean callPostJson(String url, String jsonData, String credentials) throws IOException {
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

		post.setHeader("Content-type", "application/json");
		post.setHeader("Authorization", "Basic " + credentials);

		StringBuffer result = new StringBuffer();
		HttpClientCallBean resultBean = new HttpClientCallBean();
		try {
			post.setEntity(new StringEntity(jsonData, "UTF-8"));
			HttpResponse resCacheDMS;
			resCacheDMS = client.execute(post);
			resultBean.setStatusCode(resCacheDMS.getStatusLine().getStatusCode());
			resultBean.setStatusMsg(resCacheDMS.getStatusLine().getReasonPhrase());
			BufferedReader rd;
			rd = new BufferedReader(new InputStreamReader(resCacheDMS.getEntity().getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			resultBean.setContentBody(result.toString());
		} catch(Exception e) {
			//log.severe("[HubService - HttpClientCallBean] Unexpected error: ");
			e.printStackTrace();
		} finally {
			client.close();
		}        
		return resultBean;
	}

	public static AggiornaCacheDMSBean aggiornaCacheDMS(  String ot, String codiceDocumento, String idMovimento, String codiceDMS) throws Exception {

		HttpClientCallBean responseCallBean = new HttpClientCallBean();
		AggiornaCacheDMSBean result = new  AggiornaCacheDMSBean();
		ReadConfigFile p = new ReadConfigFile();
		Properties props = p.getProperty(false);

		int sleepTime = 1;//secondi
		int tries = 0;	
		String jsonData = "{ \"username\":\"" + props.getProperty("AggiornaCacheDMS_userName") + "\","
				+ "\"ot\":\"" + ot + "\","
				+ "\"serviceCaller\":\"" + props.getProperty("AggiornaCacheDMS_serviceCaller") + "\","
				+ "\"codiceDocumento\":\"" + codiceDocumento + "\","
				+ "\"idMovimento\":\"" + idMovimento + "\","
				+ "\"codiceDMS\":\"" + codiceDMS + "\"}";

		log.info(jsonData);
		log.info(props.getProperty("AggiornaCacheDMS_credentials"));
		String credentials = Base64.getEncoder().encodeToString((props.getProperty("AggiornaCacheDMS_credentials") ).getBytes(StandardCharsets.UTF_8));        
		log.info("UTF 8: "+credentials);
		while (tries < 3){
			responseCallBean = callPostJson(props.getProperty("AggiornaCacheDMS_url") , jsonData, credentials);

			if(responseCallBean.getStatusCode() == HttpStatus.SC_OK) {
				//result.setStatusCode(statusCode);
				if(ot.equals("JSON")) {
					JSONObject obj = new JSONObject(responseCallBean.getContentBody());					
					result.setCode(obj.get("code").toString());
					result.setDescription(obj.get("description").toString());					
				} else if(ot.equals("XML")){

					String tagName = "code";
					result.setCode(responseCallBean.getContentBody().split("<"+tagName+">")[1].split("</"+tagName+">")[0]);
					tagName = "description";
					result.setDescription(responseCallBean.getContentBody().split("<"+tagName+">")[1].split("</"+tagName+">")[0]);
					//org.w3c.dom.Document doc = convertStringToXMLDocument(responseCallBean.getContentBody());
					//result.setCode(doc.getNodeValue()  XmlUtilities.executeXPathString(doc, "/EsitoDTO/code"));					
					//result.setDescription(XmlUtilities.executeXPathString(doc, "/EsitoDTO/description"));					
				}					
				return result;
			} else {
				//retry con attesa sleepTime
				TimeUnit.SECONDS.sleep(sleepTime);
				tries += 1;
			}	
		}		
		result.setCode(String.valueOf(responseCallBean.getStatusCode()));
		result.setDescription(responseCallBean.getStatusMsg().concat(" - ").concat(responseCallBean.getContentBody()));

		return result;
	}

	private static org.w3c.dom.Document convertStringToXMLDocument(String xmlString) throws Exception
	{
		//Parser that produces DOM object trees from XML content
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//API to obtain DOM Document instance
		DocumentBuilder builder = null;
		//Create DocumentBuilder with default configuration
		builder = factory.newDocumentBuilder();
		//Parse the content to Document object
		return builder.parse(new InputSource(new StringReader(xmlString)));
	}


	//"dfc3beb8-c574-41f8-8c6c-2e577265385e"
	public static Document getMovimentazionePolizza(String idDocumento) throws RemoteException,Exception{
		WebRainbowUtility util = new WebRainbowUtility();
		byte[] resultDoc = util.getDocumento(idDocumento);
		//OpenOption options
		//Path path = Paths.get("C:\\Users\\oem\\Documents\\Cut&Copy\\test.pdf");
		//Files.write(path, resultDoc);
		Document doc  = new Document(resultDoc);
		return doc;
	}

}