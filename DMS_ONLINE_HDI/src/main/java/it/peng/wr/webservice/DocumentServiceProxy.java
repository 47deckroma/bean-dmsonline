package it.peng.wr.webservice;

public class DocumentServiceProxy implements it.peng.wr.webservice.DocumentService {
  private String _endpoint = null;
  private it.peng.wr.webservice.DocumentService documentService = null;
  
  public DocumentServiceProxy() {
    _initDocumentServiceProxy();
  }
  
  public DocumentServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initDocumentServiceProxy();
  }
  
  private void _initDocumentServiceProxy() {
    try {
      documentService = (new it.peng.wr.webservice.DocumentLocator()).getDocumentServicePort();
      if (documentService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)documentService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)documentService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (documentService != null)
      ((javax.xml.rpc.Stub)documentService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.peng.wr.webservice.DocumentService getDocumentService() {
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService;
  }
  
  public java.lang.String login(java.lang.String username, java.lang.String password, java.lang.String workspace) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.login(username, password, workspace);
  }
  
  public void logout(java.lang.String token) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    documentService.logout(token);
  }
  
  public java.lang.String[] getEntityChildren(java.lang.String token, java.lang.String entityId, java.lang.String childrenType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getEntityChildren(token, entityId, childrenType);
  }
  
  public java.lang.String[] findEntitiesWithExplicitQuery(java.lang.String token, java.lang.String queryString, java.lang.String queryLimit, java.lang.String timeout) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.findEntitiesWithExplicitQuery(token, queryString, queryLimit, timeout);
  }
  
  public java.lang.String[] findEntities(java.lang.String token, java.lang.String entityType, java.lang.String basePath, it.peng.wr.webservice.QueryCondition[] query) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.findEntities(token, entityType, basePath, query);
  }
  
  public it.peng.wr.webservice.FieldData[] getEntity(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getEntity(token, entityId);
  }
  
  public java.lang.String getEntityPath(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getEntityPath(token, entityId);
  }
  
  public java.lang.String[] getFieldsDefinition(java.lang.String token, java.lang.String entityType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getFieldsDefinition(token, entityType);
  }
  
  public java.lang.String[] getChildrenType(java.lang.String token, java.lang.String entityType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getChildrenType(token, entityType);
  }
  
  public it.peng.wr.webservice.FieldData[] getMetaProperties(java.lang.String token, java.lang.String entityType, java.lang.String property) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getMetaProperties(token, entityType, property);
  }
  
  public java.lang.String getParentEntity(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getParentEntity(token, entityId);
  }
  
  public java.lang.Object[] getFieldValue(java.lang.String token, java.lang.String entityId, java.lang.String property) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getFieldValue(token, entityId, property);
  }
  
  public java.lang.String[] getRelated(java.lang.String token, java.lang.String entityId, java.lang.String entityType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getRelated(token, entityId, entityType);
  }
  
  public java.lang.String resolveNamedIdentifier(java.lang.String token, java.lang.String expression) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.resolveNamedIdentifier(token, expression);
  }
  
  public it.peng.wr.webservice.FieldData[] getEntityDefinition(java.lang.String token, java.lang.String entityType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getEntityDefinition(token, entityType);
  }
  
  public java.lang.String insertEntity(java.lang.String token, java.lang.String entityType, it.peng.wr.webservice.FieldData[] metaData, java.lang.String parentId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.insertEntity(token, entityType, metaData, parentId);
  }
  
  public java.lang.String fastInsertEntity(java.lang.String token, java.lang.String entityType, it.peng.wr.webservice.FieldData[] metaData, java.lang.String parentId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.fastInsertEntity(token, entityType, metaData, parentId);
  }
  
  public java.lang.String[] fastInsertEntities(java.lang.String token, java.lang.String entityType, it.peng.wr.webservice.FieldData[][] metaDataListList, java.lang.String parentId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.fastInsertEntities(token, entityType, metaDataListList, parentId);
  }
  
  public boolean deleteEntity(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.deleteEntity(token, entityId);
  }
  
  public java.lang.String getEntityMode(java.lang.String token, java.lang.String entityType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getEntityMode(token, entityType);
  }
  
  public java.lang.String getEntityType(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getEntityType(token, entityId);
  }
  
  public java.lang.Object[] pathParser(java.lang.String token, java.lang.String expression, java.lang.String startEntityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.pathParser(token, expression, startEntityId);
  }
  
  public java.lang.String[] getEntityFromPath(java.lang.String token, java.lang.String path, java.lang.String startEntityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getEntityFromPath(token, path, startEntityId);
  }
  
  public java.lang.String updateEntity(java.lang.String token, java.lang.String entityId, it.peng.wr.webservice.FieldData[] metaData) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.updateEntity(token, entityId, metaData);
  }
  
  public java.lang.String fastUpdateEntity(java.lang.String token, java.lang.String entityId, it.peng.wr.webservice.FieldData[] metaData) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.fastUpdateEntity(token, entityId, metaData);
  }
  
  public it.peng.wr.webservice.FieldData[] getEntityVersions(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.getEntityVersions(token, entityId);
  }
  
  public void signalWorkflow(java.lang.String token, java.lang.String entityId, java.lang.String transition) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    documentService.signalWorkflow(token, entityId, transition);
  }
  
  public java.lang.String executeRule(java.lang.String token, java.lang.String ruleName, it.peng.wr.webservice.FieldData[] parameters) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.executeRule(token, ruleName, parameters);
  }
  
  public boolean createWorkflowInstance(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException{
    if (documentService == null)
      _initDocumentServiceProxy();
    return documentService.createWorkflowInstance(token, entityId);
  }
  
  
}