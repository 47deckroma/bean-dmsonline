/**
 * DocumentService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.peng.wr.webservice;

public interface DocumentService extends java.rmi.Remote {
    public java.lang.String login(java.lang.String username, java.lang.String password, java.lang.String workspace) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public void logout(java.lang.String token) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String[] getEntityChildren(java.lang.String token, java.lang.String entityId, java.lang.String childrenType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String[] findEntitiesWithExplicitQuery(java.lang.String token, java.lang.String queryString, java.lang.String queryLimit, java.lang.String timeout) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String[] findEntities(java.lang.String token, java.lang.String entityType, java.lang.String basePath, it.peng.wr.webservice.QueryCondition[] query) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public it.peng.wr.webservice.FieldData[] getEntity(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String getEntityPath(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String[] getFieldsDefinition(java.lang.String token, java.lang.String entityType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String[] getChildrenType(java.lang.String token, java.lang.String entityType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public it.peng.wr.webservice.FieldData[] getMetaProperties(java.lang.String token, java.lang.String entityType, java.lang.String property) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String getParentEntity(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.Object[] getFieldValue(java.lang.String token, java.lang.String entityId, java.lang.String property) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String[] getRelated(java.lang.String token, java.lang.String entityId, java.lang.String entityType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String resolveNamedIdentifier(java.lang.String token, java.lang.String expression) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public it.peng.wr.webservice.FieldData[] getEntityDefinition(java.lang.String token, java.lang.String entityType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String insertEntity(java.lang.String token, java.lang.String entityType, it.peng.wr.webservice.FieldData[] metaData, java.lang.String parentId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String fastInsertEntity(java.lang.String token, java.lang.String entityType, it.peng.wr.webservice.FieldData[] metaData, java.lang.String parentId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String[] fastInsertEntities(java.lang.String token, java.lang.String entityType, it.peng.wr.webservice.FieldData[][] metaDataListList, java.lang.String parentId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public boolean deleteEntity(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String getEntityMode(java.lang.String token, java.lang.String entityType) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String getEntityType(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.Object[] pathParser(java.lang.String token, java.lang.String expression, java.lang.String startEntityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String[] getEntityFromPath(java.lang.String token, java.lang.String path, java.lang.String startEntityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String updateEntity(java.lang.String token, java.lang.String entityId, it.peng.wr.webservice.FieldData[] metaData) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String fastUpdateEntity(java.lang.String token, java.lang.String entityId, it.peng.wr.webservice.FieldData[] metaData) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public it.peng.wr.webservice.FieldData[] getEntityVersions(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public void signalWorkflow(java.lang.String token, java.lang.String entityId, java.lang.String transition) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public java.lang.String executeRule(java.lang.String token, java.lang.String ruleName, it.peng.wr.webservice.FieldData[] parameters) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
    public boolean createWorkflowInstance(java.lang.String token, java.lang.String entityId) throws java.rmi.RemoteException, it.peng.wr.webservice.WebServiceException;
}
