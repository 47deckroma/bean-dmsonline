/**
 * FindEntitiesWithExplicitQuery.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.peng.wr.webservice;

public class FindEntitiesWithExplicitQuery  implements java.io.Serializable {
    private java.lang.String token;

    private java.lang.String queryString;

    private java.lang.String queryLimit;

    private java.lang.String timeout;

    public FindEntitiesWithExplicitQuery() {
    }

    public FindEntitiesWithExplicitQuery(
           java.lang.String token,
           java.lang.String queryString,
           java.lang.String queryLimit,
           java.lang.String timeout) {
           this.token = token;
           this.queryString = queryString;
           this.queryLimit = queryLimit;
           this.timeout = timeout;
    }


    /**
     * Gets the token value for this FindEntitiesWithExplicitQuery.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this FindEntitiesWithExplicitQuery.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }


    /**
     * Gets the queryString value for this FindEntitiesWithExplicitQuery.
     * 
     * @return queryString
     */
    public java.lang.String getQueryString() {
        return queryString;
    }


    /**
     * Sets the queryString value for this FindEntitiesWithExplicitQuery.
     * 
     * @param queryString
     */
    public void setQueryString(java.lang.String queryString) {
        this.queryString = queryString;
    }


    /**
     * Gets the queryLimit value for this FindEntitiesWithExplicitQuery.
     * 
     * @return queryLimit
     */
    public java.lang.String getQueryLimit() {
        return queryLimit;
    }


    /**
     * Sets the queryLimit value for this FindEntitiesWithExplicitQuery.
     * 
     * @param queryLimit
     */
    public void setQueryLimit(java.lang.String queryLimit) {
        this.queryLimit = queryLimit;
    }


    /**
     * Gets the timeout value for this FindEntitiesWithExplicitQuery.
     * 
     * @return timeout
     */
    public java.lang.String getTimeout() {
        return timeout;
    }


    /**
     * Sets the timeout value for this FindEntitiesWithExplicitQuery.
     * 
     * @param timeout
     */
    public void setTimeout(java.lang.String timeout) {
        this.timeout = timeout;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindEntitiesWithExplicitQuery)) return false;
        FindEntitiesWithExplicitQuery other = (FindEntitiesWithExplicitQuery) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.queryString==null && other.getQueryString()==null) || 
             (this.queryString!=null &&
              this.queryString.equals(other.getQueryString()))) &&
            ((this.queryLimit==null && other.getQueryLimit()==null) || 
             (this.queryLimit!=null &&
              this.queryLimit.equals(other.getQueryLimit()))) &&
            ((this.timeout==null && other.getTimeout()==null) || 
             (this.timeout!=null &&
              this.timeout.equals(other.getTimeout())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getQueryString() != null) {
            _hashCode += getQueryString().hashCode();
        }
        if (getQueryLimit() != null) {
            _hashCode += getQueryLimit().hashCode();
        }
        if (getTimeout() != null) {
            _hashCode += getTimeout().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindEntitiesWithExplicitQuery.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.wr.peng.it/", "findEntitiesWithExplicitQuery"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("", "token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queryString");
        elemField.setXmlName(new javax.xml.namespace.QName("", "queryString"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queryLimit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "queryLimit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeout");
        elemField.setXmlName(new javax.xml.namespace.QName("", "timeout"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
