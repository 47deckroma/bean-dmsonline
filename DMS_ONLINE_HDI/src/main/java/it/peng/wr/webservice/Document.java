/**
 * Document.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.peng.wr.webservice;

public interface Document extends javax.xml.rpc.Service {
    public java.lang.String getDocumentServicePortAddress();

    public it.peng.wr.webservice.DocumentService getDocumentServicePort() throws javax.xml.rpc.ServiceException;

    public it.peng.wr.webservice.DocumentService getDocumentServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
