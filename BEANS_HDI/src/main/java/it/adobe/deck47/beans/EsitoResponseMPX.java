package it.adobe.deck47.beans;


import java.io.Serializable;
import java.util.ArrayList;
public class EsitoResponseMPX implements Serializable  {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3877064036242733825L;
	private int returnCodeInt=-100;
	private String returnCodeString="Errore generico";
	private String MPXString="";
	private org.w3c.dom.Document mpxDocument = null;
	private String CustomerSetID="";
	private ArrayList<ItemQuery> listaItemQuery= new ArrayList<ItemQuery>();
	
	
	private String codiceAgenzia, codiceDocumento,  numeroPolizza, idMovimento,  tipologiaDocumento, codiceCausale;
	
	public String getCodiceAgenzia() {
		return codiceAgenzia;
	}
	public void setCodiceAgenzia(String codiceAgenzia) {
		this.codiceAgenzia = codiceAgenzia;
	}
	public String getCodiceDocumento() {
		return codiceDocumento;
	}
	public void setCodiceDocumento(String codiceDocumento) {
		this.codiceDocumento = codiceDocumento;
	}
	public String getNumeroPolizza() {
		return numeroPolizza;
	}
	public void setNumeroPolizza(String numeroPolizza) {
		this.numeroPolizza = numeroPolizza;
	}
	public String getIdMovimento() {
		return idMovimento;
	}
	public void setIdMovimento(String idMovimento) {
		this.idMovimento = idMovimento;
	}
	public String getTipologiaDocumento() {
		return tipologiaDocumento;
	}
	public void setTipologiaDocumento(String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}
	public String getCodiceCausale() {
		return codiceCausale;
	}
	public void setCodiceCausale(String codiceCausale) {
		this.codiceCausale = codiceCausale;
	}
	public int getReturnCodeInt() {
		return returnCodeInt;
	}
	public void setReturnCodeInt(int returnCodeInt) {
		this.returnCodeInt = returnCodeInt;
	}
	public String getReturnCodeString() {
		return returnCodeString;
	}
	public void setReturnCodeString(String returnCodeString) {
		this.returnCodeString = returnCodeString;
	}
	public String getMPXString() {
		return MPXString;
	}
	public void setMPXString(String mPXString) {
		MPXString = mPXString;
	}
	public org.w3c.dom.Document getMpxDocument() {
		return mpxDocument;
	}
	public void setMpxDocument(org.w3c.dom.Document mpxDocument) {
		this.mpxDocument = mpxDocument;
	}
	public String getCustomerSetID() {
		return CustomerSetID;
	}
	public void setCustomerSetID(String customerSetID) {
		CustomerSetID = customerSetID;
	}
	public ArrayList<ItemQuery> getListaItemQuery() {
		return listaItemQuery;
	}
	public void setListaItemQuery(ArrayList<ItemQuery> listaItemQuery) {
		this.listaItemQuery = listaItemQuery;
	}
	
	
}
