package it.adobe.deck47.beans;

import java.io.Serializable;


public class ItemDelivery implements Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 3022521332148723698L;
		/**
	 * BEAN che racchiude le informazioni scambiate tra i componenti di invio e il componente di update del db
	 */
	//IDENTIFICAZIONE DEL TIPO 
	private String type="";//SMS o MAIL o ....come nella tabella T_E_TIPO
	//STATO==Esito(da mettere sul DB, che dovrà essere allineato a quello presente nella tabella T_E_CODE) ed CODICE(opzionale)
	private String status="";
	private String code="";
	
	//Per trovare il delivery da modificare c'è bisogno del customerEnvelopId, opzionale il customerSetId, ma facilita un possibile aggiornamento per aggiornare lo stato di tutto il lotto
	private String customerEnvelopeID="";
	private String customerSetID="";
	//CAMPI MAIL O SMS
	private String text="";//Testo mail?? o sms 
	private String destinatario="";//mail o numeroTel
	private String destinatarioCC="";//solo per mail
	private String errorMessage="";
	//Se vuoi inserire anche al data di invio
	 private String sendDate="";
	  	

 
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getCustomerSetID() {
			return customerSetID;
		}
		public void setCustomerSetID(String customerSetID) {
			this.customerSetID = customerSetID;
		}
		
		 
		public String getCustomerEnvelopeID() {
			return customerEnvelopeID;
		}
		public void setCustomerEnvelopeID(String customerEnvelopeID) {
			this.customerEnvelopeID = customerEnvelopeID;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public String getDestinatario() {
			return destinatario;
		}
		public void setDestinatario(String destinatario) {
			this.destinatario = destinatario;
		}
		public String getDestinatarioCC() {
			return destinatarioCC;
		}
		public void setDestinatarioCC(String destinatarioCC) {
			this.destinatarioCC = destinatarioCC;
		}
		public String getSendDate() {
			return sendDate;
		}
		public void setSendDate(String sendDate) {
			this.sendDate = sendDate;
		}
		public String getErrorMessage() {
			return errorMessage;
		}
		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
		@Override
		public String toString() {
			return "ItemDelivery [type=" + type + ", status=" + status + ", code=" + code + ", customerEnvelopeID="
					+ customerEnvelopeID + ", customerSetID=" + customerSetID + ", text=" + text + ", destinatario="
					+ destinatario + ", destinatarioCC=" + destinatarioCC + ", errorMessage=" + errorMessage
					+ ", sendDate=" + sendDate + "]";
		}

}
