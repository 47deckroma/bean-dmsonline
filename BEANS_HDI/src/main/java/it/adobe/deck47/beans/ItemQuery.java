package it.adobe.deck47.beans;

import java.io.Serializable;


public class ItemQuery implements Serializable {
	
	  	@Override
	public String toString() {
		return "ItemQuery [status=" + status + ", uploadDate=" + uploadDate + ", notifyDate=" + notifyDate + ", code="
				+ code + ", customerSetID=" + customerSetID + ", MPXSetID=" + MPXSetID + ", codRaccomandata="
				+ codRaccomandata + ", mailDate=" + mailDate +   ", customerEnvelopeID="
				+ customerEnvelopeID + ", type=" + type + "]";
	}
		/**
	 * 
	 */
	private static final long serialVersionUID = -1046704430467256988L;
		private String status="";
	  	private String uploadDate="";
	  	private String notifyDate="";
	  	private String code="";
	  	private String customerSetID="";
	  	private String MPXSetID="";
	  	private String codRaccomandata="";
	  	private String mailDate="";
	  	
	  	private String customerEnvelopeID="";
	  	private String type="";
	  	
	  	
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getUploadDate() {
			return uploadDate;
		}
		public void setUploadDate(String uploadDate) {
			this.uploadDate = uploadDate;
		}
		public String getNotifyDate() {
			return notifyDate;
		}
		public void setNotifyDate(String notifyDate) {
			this.notifyDate = notifyDate;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getCustomerSetID() {
			return customerSetID;
		}
		public void setCustomerSetID(String customerSetID) {
			this.customerSetID = customerSetID;
		}
		public String getMPXSetID() {
			return MPXSetID;
		}
		public void setMPXSetID(String mPXSetID) {
			MPXSetID = mPXSetID;
		}
		public String getCodRaccomandata() {
			return codRaccomandata;
		}
		public void setCodRaccomandata(String codRaccomandata) {
			this.codRaccomandata = codRaccomandata;
		}
		public String getMailDate() {
			return mailDate;
		}
		public void setMailDate(String mailDate) {
			this.mailDate = mailDate;
		}
		 
		public String getCustomerEnvelopeID() {
			return customerEnvelopeID;
		}
		public void setCustomerEnvelopeID(String customerEnvelopeID) {
			this.customerEnvelopeID = customerEnvelopeID;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}

}
